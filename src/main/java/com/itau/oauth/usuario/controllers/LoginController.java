package com.itau.oauth.usuario.controllers;

import com.itau.oauth.usuario.models.Cliente;
import com.itau.oauth.usuario.security.Usuario;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {

    @PostMapping
    public Cliente create(@AuthenticationPrincipal Usuario usuario) {
        Cliente cliente = new Cliente();
        cliente.setNome(usuario.getName());
        return cliente;
    }
}
